import React from 'react'


class Canvas extends React.Component {
    canvas: any = null;
    ctx: any = null;
    mousedown = false;
    oldx: number = 0;
    oldy: number = 0;
    pixels: any = null;

    componentDidMount() {
        this.context = this.canvas.getContext('2d');
        this.addMouseEvents();
        const letter: string = prompt('Слово','МАМА')||'МАМА';
        this.canvas.height = 480;
        this.canvas.width = 320 * (letter?.length || 1);
        this.context.lineWidth = 45;
        this.context.lineCap = 'round';
        this.context.font = 'bold 300px verdana';
        this.context.fillStyle = 'rgb(200, 200, 200)';
        this.context.textBaseline = 'middle';

        const centerx = (this.canvas.width - this.context.measureText(letter).width) / 2;
        const centery = this.canvas.height / 2;
        this.context.fillText(letter, centerx, centery);

        this.pixels = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
    }

    paint(x: number, y: number) {
        const colour = this.getpixelcolour(x, y);
        if (colour.a === 0) {
            this.showerror();
        } else {
            this.context.beginPath();
            if (this.oldx > 0 && this.oldy > 0) {
                this.context.moveTo(this.oldx, this.oldy);
            }
            this.context.lineTo(x, y);
            this.context.stroke();
            this.context.closePath();
            this.oldx = x;
            this.oldy = y;
        }
    }

    getpixelcolour(x: number, y: number) {
        const index = ((y * (this.pixels.width * 4)) + (x * 4));
        return {
            r: this.pixels.data[index],
            g: this.pixels.data[index + 1],
            b: this.pixels.data[index + 2],
            a: this.pixels.data[index + 3]
        };
    }

    showerror() {
        this.mousedown = false;
    }

    componentWillUnmount() {
        this.removeMouseEvents();
    }

    addMouseEvents() {
        document.addEventListener('mousedown', this.onMouseDown, false);
        document.addEventListener('mousemove', this.onMouseMove, false);
        document.addEventListener('mouseup', this.onMouseUp, false);
    }

    removeMouseEvents() {
        document.removeEventListener('mousedown', this.onMouseDown, false);
        document.removeEventListener('mousemove', this.onMouseMove, false);
        document.removeEventListener('mouseup', this.onMouseUp, false);
    }

    onMouseDown = (e: any) => {
        this.mousedown = true;
        e.preventDefault();
    };

    onMouseMove = (e: any) => {
        const x = e.clientX;
        const y = e.clientY;
        if (this.mousedown) {
            requestAnimationFrame(() => {
                this.paint(x, y);
            });
        }
    };

    onMouseUp = (e: any) => {
        this.mousedown = false;
        this.oldx = 0;
        this.oldy = 0;
        e.preventDefault();
    };

    render() {
        return <canvas width={'300px'} height={'300px'} ref={(c) => {
            this.canvas = c;
        }}/>
    }
}

export default Canvas;
